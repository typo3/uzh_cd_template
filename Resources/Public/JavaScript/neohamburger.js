class NavigationMenu {

    constructor(navigationParentId, naviContainerId, pagesContainerId, mobileOpen) {
        this.navigationParent = document.getElementById(navigationParentId)
        this.naviContainer = document.getElementById(naviContainerId)
        this.pagesContainer = document.getElementById(pagesContainerId)
        this.mobileButton = document.getElementById(mobileOpen)
        this.menuEntries = []
        this.current = null
        this.currentParent = null
        this.blackout = null
        this.pages = []

    }

    setupMenuEntriesAndEvents() {
        for (const child of this.navigationParent.children) {
            const menuEntry = new MenuEntry(null, child, this)
            menuEntry.setupChildren()
            this.menuEntries.push(menuEntry)
            child.addEventListener("mouseover", menuEntry.mouseEnter.bind(menuEntry))
        }
        this.mobileButton.addEventListener("click", this.mobileMenuOpen.bind(this))
    }

    getGridContainer() {
        const element = document.createElement("div")
        element.classList.add("subnav-grid")
        return element
    }

    getBlackoutFilter() {
        const element = document.createElement("div")
        element.classList.add("menu-blackout")
        element.addEventListener("click", this.closeMenu.bind(this))
        return element
    }

    clearMenu() {
        while (this.pagesContainer.firstChild) {
            this.pagesContainer.removeChild(this.pagesContainer.lastChild);
        }
        this.pages = []
        this.pagesContainer.style.transform = "translate(0,0)"
    }

    closeMenu(removeBlackout) {
        // Clean menu container
        this.clearMenu()
        this.naviContainer.classList.remove("open")


        if (this.blackout !== null && removeBlackout) {
            this.blackout.remove()
            this.blackout = null;
        }
        this.mobileButton.querySelector(".header-burger-open").classList.remove("hidden")
        this.mobileButton.querySelector(".header-burger-close").classList.add("hidden")
        this.current = null
        this.naviContainer.style.height = "unset"
    }

    mobileMenuOpen() {
        if (this.blackout === null) {
            this.blackout = this.getBlackoutFilter()
            document.body.prepend(this.blackout)
        } else {
            this.closeMenu(true)
            this.naviContainer.style.height = "0"
            return
        }

        console.log("Mobile Menu Open")
        const container = this.getGridContainer()
        for (const menuEntry of this.menuEntries) {
            container.append(menuEntry.getDisplayElement(true))
        }
        const menuPage = new MenuPage(container, null, this)
        this.pages.push(menuPage)
        this.naviContainer.style.left = "0px"
        this.naviContainer.style.height = "100vh"
        this.pagesContainer.append(container)
        //this.naviContainer.style.height = this.pagesContainer.offsetHeight
        this.naviContainer.classList.add("open")
        this.mobileButton.querySelector(".header-burger-open").classList.add("hidden")
        this.mobileButton.querySelector(".header-burger-close").classList.remove("hidden")
    }

    openMenu(triggerEntry) {
        // prevent multiple times triggering the same record display
        if (triggerEntry.element && this.current == triggerEntry.element.text) return
        this.closeMenu(triggerEntry.children.length == 0)
        this.current = null
        this.pages = []
        if (triggerEntry.children.length == 0) return
        if (this.blackout === null && triggerEntry.children.length > 0) {
            this.blackout = this.getBlackoutFilter()
            document.body.prepend(this.blackout)
        }


        // Get highest menu entry for positioning
        parent = triggerEntry
        while (parent.parent !== null) {
            parent = parent.parent
        }
        // Set css style left for positioning
        this.naviContainer.style.left = parent.entry.offsetLeft + "px"


        const container = this.getGridContainer()
        const parentLink = triggerEntry.getDisplayElement(false)
        container.append(parentLink)
        // Add all Menu entries
        for (const menuEntry of triggerEntry.children) {
            container.append(menuEntry.getDisplayElement(true))
        }
        const menuPage = new MenuPage(container, null, this)
        container.addEventListener("mouseenter", menuPage.mouseEnter.bind(menuPage))
        container.addEventListener("mouseleave", menuPage.mouseLeave.bind(menuPage))
        container.style.gridColumn = 1;
        this.pages.push(menuPage)
        this.pagesContainer.append(container)
        this.naviContainer.classList.add("open")
        //this.naviContainer.style.height = this.pagesContainer.offsetHeight + "px"
        container.animate([{ zoom: "80%" }, { zoom: "100%" }], { duration: 300, easing: "ease-in" })
        this.current = triggerEntry.element.text
    }

    getIcon(elementId) {
        return document.getElementById(elementId).cloneNode(true)
    }

    getBackButton() {
        const element = document.createElement("button")
        element.classList.add("btn", "btn-link", "menu-back-link")
        element.append(this.getIcon("back-button-icon"))
        const span = document.createElement("span")
        span.classList.add("back-label")
        span.textContent = "Back"
        element.append(span)
        element.addEventListener("click", this.backButtonPressed.bind(this))
        return element
    }

    openSubmenu(triggerEntry) {
        //this.clearMenu()
        const container = this.getGridContainer()
        const backLink = this.getBackButton()
        container.append(backLink)
        const parentLink = triggerEntry.getDisplayElement(false)
        container.append(parentLink)
        for (const menuEntry of triggerEntry.children) {
            container.append(menuEntry.getDisplayElement(true))
        }
        const lastMenu = this.pages.pop()
        lastMenu.deactivate()
        const menuPage = new MenuPage(container, lastMenu, this)
        container.addEventListener("mouseenter", menuPage.mouseEnter.bind(menuPage))
        container.addEventListener("mouseleave", menuPage.mouseLeave.bind(menuPage))
        this.pages.push(lastMenu, menuPage)
        container.style.gridColumn = this.pages.length
        this.pagesContainer.append(container)
        this.scrollForward()
    }

    scrollForward() {
        const pixelWidth = this.naviContainer.clientWidth

        console.log(pixelWidth + "* (" + this.pages.length + "- 1)) + px")
        this.pagesContainer.animate([
            { "transform": "translate(" + (0 - pixelWidth * (this.pages.length - 2)) + "px, 0)" },
            { "transform": "translate(" + (0 - pixelWidth * (this.pages.length - 1)) + "px, 0)" }], { duration: 200, easing: "ease-in" })
        this.pagesContainer.style.transform = "translate(" + (0 - pixelWidth * (this.pages.length - 1)) + "px, 0)"
    }

    scrollBackward() {
        const pixelWidth = this.naviContainer.clientWidth
        this.pagesContainer.animate([{ "transform": "translate(" + (0 - pixelWidth * (this.pages.length)) + "px, 0)" }, { "transform": "translate(" + (0 - pixelWidth * (this.pages.length - 1)) + "px, 0)" }], { duration: 200 })
        this.pagesContainer.style.transform = "translate(" + (0 - pixelWidth * (this.pages.length - 1)) + "px, 0)"
        setTimeout(this.removeLastPage.bind(this), 200);
        //this.pagesContainer.addEventListener(onanimationend, this.removeLastPage.bind(this))
    }

    removeLastPage() {
        console.log("remove last page", this.pagesContainer.lastChild)
        if (this.pagesContainer.children.length <= 1) return
        this.pagesContainer.removeChild(this.pagesContainer.lastChild)
    }

    backButtonPressed() {
        const page = this.pages.pop()
        page.deactivate()
        this.pages.at(-1).activate()
        /*if(page.back === null) {
            this.clearMe = true
            this.clearMenu()
        }*/
        this.scrollBackward()
        //this.naviContainer.append(page.back.pageContent)
    }
}

class MenuPage {
    constructor(pageContent, back, navigationMenu) {
        this.pageContent = pageContent
        this.back = back
        this.navigationMenu = navigationMenu
        this.mouseInside = false
        this.active = true
    }

    mouseEnter(e) {
        this.mouseInside = true
    }

    mouseLeave(e) {
        if (this.mouseInside && this.active) {
            this.navigationMenu.closeMenu(true);
        }
    }

    getPrevious() {
        this.deactivate()
        return this.back
    }

    display() {
        this.activate()
        return this.pageContent
    }

    activate() {
        this.active = true
    }

    deactivate() {
        this.active = false
    }

}

class MenuEntry {
    constructor(parent, element, navMenu) {
        this.entry = element
        this.submenu = element.querySelector("ul")
        this.expanded = false
        this.parent = parent
        this.children = []
        this.navigationMenu = navMenu
    }

    get hasSubmenu() {
        if (this.submenu) return true
        return false
    }

    setupChildren() {
        if (!this.hasSubmenu) return
        for (const child of this.submenu.children) {
            if (child.tagName === "LI") {
                const menuEntry = new MenuEntry(this, child, this.navigationMenu)
                this.children.push(menuEntry)
                if (menuEntry.hasSubmenu) {
                    menuEntry.setupChildren()
                }
            }
        }
    }

    getIcon(elementId) {
        return document.getElementById(elementId).cloneNode(true)
    }

    getDisplayElement(asEntry) {
        this.element = document.createElement("a")
        this.element.text = this.entry.querySelector("a").text
        this.element.href = this.entry.querySelector("a").href

        if (asEntry) {
            this.element.classList.add("menu-entry-link")
        } else {
            this.element.classList.add("menu-parent-link")
            this.element.append(this.getIcon("parent-link-source"))
        }

        if (this.children.length > 0 && asEntry) {
            this.element.append(this.getIcon("menu-link-expand"))
            this.element.addEventListener("click", this.openSubmenu.bind(this))
        }
        return this.element
    }

    mouseEnter(e) {
        this.navigationMenu.openMenu(this);
    }

    openSubmenu(e) {
        this.navigationMenu.openSubmenu(this)
        e.stopPropagation()
        e.preventDefault()
        return false
    }
}

class SideMenu {
    constructor(classSiteMenu) {
        this.container = document.querySelector("." + classSiteMenu)
        this.button = this.container.querySelector(".menu-latch")
        this.buttonDefaultHTML = '<span class="fas fa-chevron-right"></span>'
        this.buttonCustom = this.container.querySelector(".customMenuButton")
        if (this.buttonCustom) {
            this.buttonCustom.classList.add("hidden")
            this.buttonDefaultHTML = this.buttonCustom.innerHTML
            if (this.buttonCustom.dataset.class) {
                this.container.classList.add(this.buttonCustom.dataset.class)
            }
        }
        this.button.innerHTML = this.buttonDefaultHTML

        this.content = this.container.querySelector(".content")
        this.pageHeight = document.body.scrollHeight
        this.offsetTop = 190
        if (this.content.childNodes.length > 1) {
            // Select the single .top-menu section created by typo3 - sometimes empty tt_content
            const topMenuSection = this.content.querySelector(".top-menu");
            let hasMeaningfulContent = false;

            if (topMenuSection) {
                // Check all child nodes of the section
                // Ignore the autogenerated empty anchor
                // Don't ignore qfq inline edit buttons
                topMenuSection.childNodes.forEach((child) => {
                    if (child.nodeType === Node.ELEMENT_NODE && // Ensure it's an element
                        !(child.tagName === "A" && !child.innerHTML.trim() && !child.getAttribute("href")) // Exclude empty anchors
                    ) {
                        hasMeaningfulContent = true;
                    }
                });
            }

            // Fallback to old behaviour if there is no created section
            if (topMenuSection && hasMeaningfulContent || !topMenuSection) {
                console.log("Left row has content", this.content.childNodes.length > 1)
                this.container.classList.remove("hidden")
            }
        }
        this.button.addEventListener("click", this.openMenu.bind(this))
        window.addEventListener("scroll", this.scroll.bind(this));
        window.addEventListener("resize", this.scroll.bind(this))
        // initialize at position
        this.scroll()

        // open if requested
        if (this.content.querySelector("[data-slide-open]")) this.openMenu()
        if (this.content.querySelector("[data-class]")) this.container.classList.add(this.content.querySelector("[data-class]").dataset.class)
    }

    openMenu() {
        this.container.classList.toggle("open")
        if (this.container.classList.contains("open")) {
            this.button.innerHTML = '<span class="fas fa-chevron-left"></span>'
        } else {
            this.button.innerHTML = this.buttonDefaultHTML
        }
    }

    scroll() {
        // Requesting an animation frame prevents the top adjustment from altering the calculation
        // Else scroll fires multiple time in one animation frame and sets the position top always to 0
        window.requestAnimationFrame(this.scrollAdjustment.bind(this));
    }

    scrollAdjustment() {
        let scrollTop = this.offsetTop - window.scrollY < 1 ? 0 : this.offsetTop - window.scrollY
        this.container.style.height = "calc(100vh - " + scrollTop + "px)"
        this.container.style.maxHeight = this.pageHeight - scrollTop + "px"
        this.container.style.top = scrollTop + "px"
    }
}

class UZHUtilities {
    beautifyLinks() {
        const linkElements = document.getElementsByClassName("link-arrow")
        for (const linkElement of linkElements) {
            const linkArrow = document.createElement("span")
            linkArrow.classList.add("Icon")
            linkArrow.dataset.name = "16--link-internal"
            linkArrow.innerHTML = '<svg><use xlink:href="#16--link-internal"></use>'
            linkElement.append(linkArrow)
        }
    }

    removeEmptyLastChild() {
        //const lastChildElement = document.querySelector("nav.service ul>li:last-child")
        //if(lastChildElement) lastChildElement.remove()
        // Seems empty last child isn't happening anymore.
        const linkList = document.querySelectorAll("ul.linklist>li")
        for (const linkElement of linkList) {
            if (linkElement.innerHTML === "" || linkElement.innerHTML === " ") linkElement.remove()
        }
    }

    fixBreadcrumb() {
        const navChildren = document.querySelectorAll("nav.breadcrumb>ol>li")
        let crumbCount = 0
        let crumbRepeats = 0
        let lastRef = false
        let lastCrumb = false
        console.log("navChildren", navChildren)
        for (const listElement of navChildren) {
            crumbCount += 1
            const crumbLink = listElement.getElementsByTagName('a')[0]
            if (lastRef == crumbLink.href) {
                crumbRepeats += 1
            } else {
                lastRef = crumbLink.href
                lastCrumb = crumbLink
            }
        }

        if ((crumbRepeats > 0 && crumbCount === 2) || crumbCount == 1) {
            document.querySelector("nav.breadcrumb").remove()
            return false
        } else {
            return lastCrumb
        }
    }
}

class BreadCrumb {

    constructor(lastCrumb, displaySubmenu, onlyLastSubmenu) {
        this.lastCrumb = lastCrumb
        this.displaySubmenu = displaySubmenu
        this.onlyLastSubmenu = onlyLastSubmenu
        this.blackout = null
    }

    addBreadcrumbSubMenu() {
        if (!this.displaySubmenu) return

        const navList = document.querySelectorAll(".main-nav ul.first-level>li")
        if (this.lastCrumb && this.onlyLastSubmenu) {
            const navElement = this.reverseSearchNavList(navList, this.lastCrumb.href)
            if (navElement) {
                const childList = navElement.querySelectorAll('ul>li')
                this.createSubmenu(this.lastCrumb, childList)
            } else {
                console.log("No Nav Element with href found", this.lastCrumb)
            }
        } else {
            const navChildren = document.querySelectorAll("nav.breadcrumb>ol>li")
            var first = true
            for (const listElement of navChildren) {
                if (first) {
                    first = false
                    continue
                }
                const currentCrumb = listElement.querySelector('a:first-child')
                const navElement = this.reverseSearchNavList(navList, currentCrumb.href)
                console.log("currentCrumb", currentCrumb)
                if (navElement) {
                    console.log("Navelement", navElement)
                    const childList = navElement.querySelectorAll('ul>li')
                    this.createSubmenu(currentCrumb, childList)
                } else {
                    console.log("No Nav Element with href found", currentCrumb)
                }
            }
        }
    }

    reverseSearchNavList(navList, searchLink) {
        console.log(navList)
        for (const navChild of navList) {
            const navLink = navChild.getElementsByTagName('a')[0]
            if (searchLink == navLink.href) {
                return navChild
            } else {
                console.log("navChild", navChild)
                const subNavList = navChild.querySelectorAll('ul>li')
                if (subNavList) {
                    const result = this.reverseSearchNavList(subNavList, searchLink)
                    if (result) return result
                }
            }
        }
        return false
    }

    createSubmenu(parent, childList) {
        if (childList.length < 1) return
        console.log("Making Child submenu of", childList)
        const container = document.createElement("div")
        container.classList.add("hidden", "breadcrumb-submenu")
        const ul = document.createElement("ul")
        container.appendChild(ul)
        document.body.appendChild(container)
        var lastParent = null
        ul.appendChild(this.buildListEntry(parent, true))
        for (const child of childList) {
            if (lastParent === null) lastParent = child.parentElement
            if (lastParent === child.parentElement) {
                let link = child.getElementsByTagName('a')[0]
                const li = this.buildListEntry(link)
                ul.appendChild(li)
                console.log("Same parent", child.parentElement)
            } else {
                continue
            }

        }
        // Add button to open menu to breadcrumb

        const that = this
        const span = document.createElement("span")
        span.classList.add("Icon", "breadcrumb-menu-icon")
        span.dataset.name = "16--breadcrumb-arrow"
        span.innerHTML = '<svg><use xlink:href="#16--breadcrumb-arrow"></use></svg>'
        parent.addEventListener("click", (e) => {
            if (container.classList.contains("hidden")) {
                that.openMenu(container, span)
            } else {
                that.closeMenu(container, span)
            }
            e.preventDefault()
            return false
        })
        parent.appendChild(span)
        const rect = parent.getBoundingClientRect()
        container.style.top = rect.top + 23 + "px"
        container.style.left = rect.left - 10 + "px"
    }

    buildListEntry(navLink, firstChild) {
        const li = document.createElement("li")
        li.classList.add("flyout")
        const link = document.createElement("a")
        link.classList.add("flyout-link")
        if (firstChild) link.classList.add("bold")
        link.href = navLink.href
        link.innerHTML = navLink.innerHTML
        li.appendChild(link)
        return li
    }

    getBlackoutFilter(container, span) {
        const element = document.createElement("div")
        element.classList.add("menu-blackout")
        element.addEventListener("click", (e) => {
            this.closeMenu(container, span)
            e.preventDefault
            return false
        })
        return element
    }

    closeMenu(container, span) {
        container.classList.add("hidden")
        span.classList.remove("open")
        if (this.blackout !== null) {
            this.blackout.remove()
            this.blackout = null;
        }
    }

    openMenu(container, span) {
        container.classList.remove("hidden")
        span.classList.add("open")
        if (this.blackout == null) {
            this.blackout = this.getBlackoutFilter(container, span)
            document.body.prepend(this.blackout)
        }
    }
}

class Notify {

    constructor(apiUrl, sip, element) {
        this.apiUrl = apiUrl
        this.sip = sip
        this.notifyMenu = []
        this.issues = []
        this.element = element
        this.element.parentElement.addEventListener("click", () => { console.log("Button clicked"); this.handleMenu() })
    }

    async getData() {
        const response = await fetch(this.apiUrl + 'typo3conf/ext/qfq/Classes/Api/dataReport.php?s=' + this.sip);
        if (!response.ok) {
            throw new Error(`Response status: ${response.status}`);
        }
        console.log(response)
        const json = await response.json();
        console.log(json);
        this.setIssues(json)
    }

    setIssues(json) {
        if (!json.Issues) return false
        const now = new Date()
        json.Issues.forEach((issue) => {
            issue.reminderDate = new Date(issue.reminderDate)
            let diff = now - issue.reminderDate
            diff = Math.floor(diff / 1000 / 60)
            if (diff > -60) this.showReminder(issue)
            this.issues.push(issue)
        });
        console.log(this.issues)
        if (this.issues.length > 0) {
            this.element.append(this.getBadge(this.issues.length))
        }
    }

    showReminder(issue) {
        const container = this.getAlertContainer()
        const body = document.createElement("p")
        body.classList.add("body")
        const title = document.createElement("p")
        title.classList.add("title")
        title.innerText = issue.title
        body.innerHTML = issue.note
        container.append(title)
        container.append(body)
        const buttons = document.createElement("p")
        buttons.classList.add("buttons")
        const buttonGroup = document.createElement("div")
        buttonGroup.classList.add("btn-group")
        buttons.append(buttonGroup)
        container.append(buttons)
        buttonGroup.append(this.getButton(issue.doneSip, 'Done', container, false))
        buttonGroup.append(this.getButton(issue.postponeSip, 'Postpone', container, false))
        buttonGroup.append(this.getButton(false, 'Close', container, false))
        document.body.append(container)
    }

    handleMenu() {
        const element = document.getElementById('notify-popupmenu')
        if(element) {
            console.log("Remove Menu", element)
            element.remove()
        } else {
            console.log("Open Menu")
            const rect = this.element.getBoundingClientRect();
            document.body.append(this.getPopUpMenu(rect.bottom + + window.scrollY + 20, rect.right - 350 + 28.5))
        }

    }

    getAlertContainer() {
        const container = document.createElement("div")
        container.classList.add("border-info", "alert-interactive", "removeMe")
        container.setAttribute("role", "alert")
        container.style.zIndex = 1000
        return container
    }

    getBadge(number) {
        const container = document.createElement("span")
        container.classList.add("badge", "notify-badge")
        container.textContent = number
        return container
    }

    getButton(sip, labelText, container, additionalClass) {
        const button = document.createElement("a")
        button.classList.add("btn", "btn-default")
        button.setAttribute("role", "button")
        if(additionalClass) button.classList.add(additionalClass)
        button.textContent = labelText
        const uri = this.apiUrl + 'typo3conf/ext/qfq/Classes/Api/dataReport.php?s=' + sip
        button.addEventListener("click", () => {
            if (sip) {
                this.getResponse(uri, container)
            } else {
                container.remove()
            }
        })
        return button
    }

    async getResponse(url, container) {
        const response = await fetch(url)

        if (!response.ok) {
            throw new Error(`Response status: ${response.status}`);
        }
        const json = await response.json();
        container.remove()
    } catch(error) {
        console.error(error.message);
    }

    getPopUpMenu(top, left) {
        const container = document.createElement("div")
        container.classList.add("popup-menu")
        container.style.top = top + 'px'
        container.style.left = left + 'px'
        container.id = 'notify-popupmenu'
        this.issues.forEach((issue) => {
            container.append(this.getIssueEntry(issue))
        })
        return container
    }

    getIssueEntry(issue) {
        const container = document.createElement("div")
        container.classList.add("popup-entry-notify")
        const text = document.createElement("p")
        text.classList.add("title")
        text.textContent = issue.title
        container.append(text)
        const date = document.createElement("span")
        date.classList.add("date")
        date.textContent = issue.reminderDate.toLocaleString("de-CH", { timeZone: "UTC" })
        container.append(date)
        container.append(this.getButton(issue.doneSip, 'Done', container, 'btn-small'))
        container.append(this.getButton(issue.postponeSip, 'Postpone', container, 'btn-small'))
        return container
    }

}


const navigationMenu = new NavigationMenu("nav-first-level-top", "sub-nav-container-main", "nav-pages-container", "js-header-burger")
navigationMenu.setupMenuEntriesAndEvents()
const sideMenu = new SideMenu("side-menu")
const utils = new UZHUtilities()
utils.beautifyLinks()
utils.removeEmptyLastChild()
const lastRef = utils.fixBreadcrumb()
const breadcrumb = new BreadCrumb(lastRef, true, false)
breadcrumb.addBreadcrumbSubMenu()

const notifyElement = document.getElementById("cd-notify-menu")
if (notifyElement && notifyElement.dataset.id) {
    const notifyConfig = document.getElementById(notifyElement.dataset.id)
    const notify = new Notify(notifyConfig.dataset.url, notifyConfig.dataset.sip, notifyElement)
    notify.getData()
}

console.log("%cUZH CD Navigation loaded.", "color: rgb(0, 40, 165); font-family: sans-serif;")
