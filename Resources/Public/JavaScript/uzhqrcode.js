/**
 * Created by bbaer on 4/6/17.
 */

/* @depend jquery.js
 * @depend jquery.qrcode.js
 * @depend qrcode.js */

$(document).ready(function() {
    $(".qrcodeInfos").each(
      function() {
        var width = $(this).data("width");
        var height = $(this).data("height");
        var meCard = $(this).data("mecard");
        var targetId = $(this).data("target");

        $("#" + targetId).qrcode({ width: width, height: height, text: meCard });
      });
});
