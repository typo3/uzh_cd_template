all: archive

archive: clean uzh_cd_2023.zip

uzh_cd_2023.zip: Configuration Resources ext_emconf.php ext_tables.php ext_icon.gif ext_localconf.php composer.json
	zip -r $@ $^

clean:
	rm -f uzh_cd_2023.zip
