[![pipeline status](https://git.math.uzh.ch/typo3/uzh_cd_template/badges/master/pipeline.svg)](https://git.math.uzh.ch/typo3/uzh_cd_template/commits/master)

UZH Responsive Template for Typo3
=================================

We composed a standard typo3 extension containing our UZH CD for typo3.

Features
--------

 * Easier Installation and Maintenance for our Template
 * CDNs for bootstrap and jquery. (Can be changed via variables)
 * Responsive
 * Variable based config that can be configured for each site in typo3 (See Configuration)
 * SVG UZH Logo in english and german
 * Includes Palatino Linotype as Webfont.
 * Tested with Typo3 9.4 to 13.4
 * Designed to work with QFQ Extension out of the Box

Changes
-------
Important is that cd.fullWidth.pidList works now entirely different.

Before the new notations introduced in 9.4 and made default by 10, you could just list Page Ids: 5,10
Now you have to use in, and in can't compare arrays to arrays, so for every single pageId a separate condition has to be set:
```
cd.fullWidth.pidList = 5 in tree.rootLineIds || 10 in tree.rootLineIds
```
Same applies for horizontalNav:
```
cd.navigation.horizContNav.pidList = 1 in tree.rootLineIds || 2 in tree.rootLineIds
```
Installation
------------

 * Get the newest fluid build from: https://www.math.uzh.ch/repo/?dir=uzhcd
  * prefluid is used for older typo3 instances
 * Upload the zip file in the extension menu of typo3
 * Create a template entry on the top site in the tree
 * Set Options: Clear: Constants, Setup and Rootlevel
 * Include CSS Styled Content
 * Include UZH Corporate Design

Configuration
-------------
The CD (incl. the QFQ-extension) should work out of the box once installed.
To change or define CD variables, open the template entry created above.
and modify the Section "Constants" on the "General"-Tab.
The following example gives a typical configuration.
```
cd.stylesheet = fileadmin/yourtemplate.css
cd.headerTitle.value = Title in header area
cd.pageTitle.value = Title of the Site

# Meta Tags
cd.meta.keywords = Education,Official,University,Zurich,Institute,Mathematics,Universität,Zürich,Mathematik,Institut
cd.meta.description = Official Homepage of the Mathematics Institute at the University of Zurich
cd.meta.robots =INDEX,FOLLOW

cd.pageTitle.pid = 81
cd.meta.home.pid = 81 # Please always set this!
cd.search.pid = 0
# All pages with the given pid (and below) will be displayed in full width
cd.fullWidth.pidList = 5 in tree.rootLineIds || 10 in tree.rootLineIds



# Deactivate the english switch.
cd.english.link = 0
# Set the standard headerLogo & footerLogo to english, using the internal headerLogo value.
cd.headerLogo.value = {$cd.e.headerLogo.value}
cd.footerLogo.value = {$cd.e.footerLogo.value}
```

Sidebar
-------

The former "left" column appears in a sidebar / sidemenu that can be opened via a sticky button on the left side.

You can customize this sidemenu:

```
<div class="customButton" data-class="danger"><span class="fas fa-user"></span></div>
```

The content of a html element with the class customButton changes what is displayed on the button to open the menu. 
data-class="" adds a class to the sidebar, info / success / warning / danger are predefined examples that change the color of the button and the border.

Adding data-slide-open to any element inside the left column changes the menu to be open by default.

```
<div class="alert alert-info" data-slide-open>Important Information!</div>
```

Custom CSS
-----------

An example custom CSS can be found in `typo3conf/ext/uzh_cd_2023/Resources/Public/Css/example.custom.css`.
It is included by default (`cd.stylesheet`). Simply uncomment the changes you want and add further. This file will be updated.

If you simply want to remove the header image, set:
```
cd.stylesheet = EXT:uzh_cd_2023/Resources/Public/Css/no_header_image.css
```

Install additional CSS or JS Files
----------------------------------

If you need to include additional CSS or JS files, you can do so
in the Constant of the template.

```
# Only needed for fullCalendar.js
#cd.extra.css.file06 = typo3conf/ext/qfq/Resources/Public/Css/fullcalendar.min.css

# Only needed for fullCalendar.js
#cd.extra.js.file12 = typo3conf/ext/qfq/Resources/Public/JavaScript/fullcalendar.min.js
```

(Please note: The main CSS and JS files needed for QFQ are included by default.
You should not read them manually. 
Only the `fullcalendar` files should be added, when needed.)

All Constant Options
--------------------
The listing below shows all available constants and their default value. 
Only include the ones you want to modify into your template.
```$xslt
### All Options

# bootstrap sources
cd.bootstrap.stylesheet = EXT:uzh_cd_2023/Resources/Public/Css/bootstrap.min.css
cd.bootstrap.cd.stylesheet = EXT:uzh_cd_2023/Resources/Public/Css/bootstrap.cd.css
cd.bootstrap.js = EXT:uzh_cd_2023/Resources/Public/js/bootstrap.min.js

# jquery
cd.jquery.js = EXT:uzh_cd_2023/Resources/Public/js/jquery.min.js

# Allgemeine Einstellungen: Lokales Stylesheet für eigene Definitionen, Beispiel: fileadmin/templates/custom.css
cd.stylesheet = EXT:uzh_cd_2023/Resources/Public/Css/example.custom.css

# CSS Dateien fuer QFQ (set to empty or replace, if you use it without QFQ)
cd.qfq.css.file01 = typo3conf/ext/qfq/Resources/Public/Css/bootstrap-theme.min.css
cd.qfq.css.file02 = typo3conf/ext/qfq/Resources/Public/Css/qfq-bs.css
cd.qfq.css.file03 = typo3conf/ext/qfq/Resources/Public/Css/tablesorter-bootstrap.css
cd.qfq.css.file04 = typo3conf/ext/qfq/Resources/Public/Css/font-awesome.min.css
cd.qfq.css.file05 = typo3conf/ext/qfq/Resources/Public/Css/bootstrap-datetimepicker.min.css
cd.qfq.css.file06 = typo3conf/ext/qfq/Resources/Public/Css/codemirror.css

# JS Dateien fuer QFQ (set to empty or replace, if you use it without QFQ)
cd.qfq.js.file01 = typo3conf/ext/qfq/Resources/Public/JavaScript/validator.min.js
cd.qfq.js.file02 = typo3conf/ext/qfq/Resources/Public/JavaScript/tinymce.min.js
cd.qfq.js.file03 = typo3conf/ext/qfq/Resources/Public/JavaScript/EventEmitter.min.js
cd.qfq.js.file04 = typo3conf/ext/qfq/Resources/Public/JavaScript/qfq.min.js
cd.qfq.js.file05 = typo3conf/ext/qfq/Resources/Public/JavaScript/typeahead.bundle.min.js
cd.qfq.js.file06 = typo3conf/ext/qfq/Resources/Public/JavaScript/jquery.tablesorter.combined.min.js
cd.qfq.js.file07 = typo3conf/ext/qfq/Resources/Public/JavaScript/jquery.tablesorter.pager.min.js
cd.qfq.js.file08 = typo3conf/ext/qfq/Resources/Public/JavaScript/widget-columnSelector.min.js
cd.qfq.js.file09 = typo3conf/ext/qfq/Resources/Public/JavaScript/moment.min.js
cd.qfq.js.file10 = typo3conf/ext/qfq/Resources/Public/JavaScript/bootstrap-datetimepicker.min.js
cd.qfq.js.file11 = typo3conf/ext/qfq/Resources/Public/JavaScript/codemirror.min.js
cd.qfq.js.file12 = typo3conf/ext/qfq/Resources/Public/JavaScript/code-mirror-mode/sql/sql.min.js

# Custom CSS Files for plugins, etc.
cd.extra.css.file01 =
...
cd.extra.css.file19 =

# Custom Javascript Files for plugins, etc. (Will be included at bottom of page)
cd.extra.js.file01 =
...
cd.extra.js.file19 =

# Favicon
cd.favicon = EXT:uzh_cd_2023/Resources/Public/Icons/favicon.ico

# Meta Optionen
cd.meta.keywords = Education,Official,Universität,Zürich,University,Zurich
cd.meta.description = Homepage belonging to the University of Zurich
cd.meta.robots =INDEX,FOLLOW
cd.meta.charset = UTF-8

# Ein- und Ausschalten des Sprachumschalters (englisch)
cd.english.link = 0

# Titel im Header-Bereich für die Homepage (deutsch)
cd.headerTitle.value = Universität Zürich
# Titel im Header-Bereich für die Homepage (englisch)
cd.e.headerTitle.value = University of Zurich


cd.headerLogo.value = <a href="http://www.uzh.ch" data-toggle="tooltip" data-placement="bottom" title="Universität Zürich" class="Logo"><img alt="Logo Universität Zürich" src="typo3conf/ext/uzh_cd_2023/Resources/Public/Images/uzh_logo_d_pos.svg" class="uzh-logo" /></a>
cd.e.headerLogo.value = <a href="http://www.uzh.ch" data-toggle="tooltip" data-placement="bottom" title="University of Zurich" class="Logo"><img alt="Logo University of Zurich" src="typo3conf/ext/uzh_cd_2023/Resources/Public/Images/uzh_logo_e_pos.svg" class="uzh-logo" /></a>

cd.headerLogo.Print.value = <a href="http://www.uzh.ch" data-toggle="tooltip" data-placement="bottom" title="Universität Zürich"><img alt="Logo Universität Zürich" src="typo3conf/ext/uzh_cd_2023/Resources/Public/Images/uzh_logo_d_pos.png" class="uzh-logo" /></a>
cd.e.headerLogo.Print.value = <a href="http://www.uzh.ch" data-toggle="tooltip" data-placement="bottom" title="University of Zurich"><img alt="Logo University of Zurich" src="typo3conf/ext/uzh_cd_2023/Resources/Public/Images/uzh_logo_e_pos.png" class="uzh-logo" /></a>

# New logos in footer, can be multiple. all need the class footer-logo.
cd.footerLogos.value = <a class="footer-logo" href="https://www.uzh.ch"><img src="typo3conf/ext/uzh_cd_2023/Resources/Public/Images/uzh_logo_d_pos.svg" alt="Logo"></a>
cd.e.footerLogos.value = <a class="footer-logo" href="https://www.uzh.ch"><img src="typo3conf/ext/uzh_cd_2023/Resources/Public/Images/uzh_logo_e_pos.svg" alt="Logo"></a>

# Used by eulerzentrum (tbd)
cd.header.sub.value =

# Fester Titel für die Homepage rechts vom Logo (deutsch)
cd.pageTitle.value = Institut fuer ..
# Fester Titel für die Homepage rechts vom Logo (englisch)
cd.e.pageTitle.value = Institute of ..

# Page-IDs für Links: ID der Seite mit der der Titel verlinkt wird
cd.pageTitle.pid = 0

# Fester Subtitel für die Homepage rechts vom Logo (deutsch)
cd.subTitle.value = 
# Fester Subtitel für die Homepage rechts vom Logo (englisch)
cd.e.subTitle.value =

# Page-IDs für Links: ID der Seite mit der der Untertitel verlinkt wird (untertitel wird nur angezeigt wenn gesetzt)
cd.subTitle.pid = 0

# ID der Seite mit der der Home-Link verlinkt wird (ausschalten mit '0')
cd.meta.home.pid = 0

# Liste von IDs die in der Meta-Navigation angezeigt werden (ausschalten mit '0')
cd.meta.navigation.pidList = 0

# Footer Navigation (Impressum, Kontakt, ...) neben dem UZH Logo
cd.footer.navigation.pidList = 0

# Liste von IDs bei denen die Unterseiten als horizontale Content-Navigation angezeigt werden (KEINE Leerzeichen!) (todo)
cd.navigation.horizContNav.pidList = 0 in tree.rootLineIds


# ID der Seite auf der die Suche läuft
cd.search.pid = 0

# IP-Range bei dem die eingeschränkten Inhalte ausgegeben werden (not tested/todo)
cd.restrictedIPRange =

# ID der Seite von welcher die Unterseiten als zweite Navigation erscheinen sollen
cd.navigation.showChildNavigationUid = 0
```


Custom Login Box
----------------

As 'template' record on a specific page. Section "Setup":
```

### Template for Login
#plugin.tx_felogin_pi1.templateFile = EXT:uzh_cd_2023/Resources/Public/Templates/template_login.html
plugin.tx_felogin_pi1.templateFile = fileadmin/template/template_login.html
#plugin.tx_felogin_pi1.errorMessage_stdWrap.wrap = <div class="alert alert-danger" role="alert">|</div>

plugin.tx_felogin_pi1._LOCAL_LANG.default {
   username = UZH Shortname oder E-Mail*
   password = Passwort
   ll_welcome_message = 
}

plugin.tx_felogin_pi1._LOCAL_LANG.en {
   username = UZH Shortname or E-Mail*
   password = Password
}

plugin.tx_felogin_pi1.logoutMessage_stdWrap = 

```

Advanced Customization
----------------------

Check in `typo3conf/ext/uzh_cd_2023/Resources/Private/Templates/` files  `template.html,template_letter.html,template_print_header.html,template_printview.html`. 

E.g. in `template_printview.html` you'll find `cd.footer.footerP` for the printview footer. To customize it, select in T3: Backend  `Template`, select the page, select 'Setup', navigate to `cd > footer > footerP`, choose the element to change.
