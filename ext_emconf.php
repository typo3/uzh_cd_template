<?php

$EM_CONF['uzh_cd_2023'] = array(
	'title' => 'UZH CD 2023',
	'description' => 'UZH Corporate Design site template for Typo3',
	'category' => 'distribution',
	'author' => 'Benjamin Baer',
	'author_email' => 'support@math.uzh.ch',
	'dependencies' => 'fluid,fluid_styled_content',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearcacheonload' => '0',
	'version' => '24.12.02',
	'constraints' => array(
		'depends' => array(
                    'typo3' => '10.0.0-13.4.99',
#                    'extbase' => ''
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
