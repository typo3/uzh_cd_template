<?php
defined('TYPO3') or
defined('TYPO3_MODE') or
die('UZH CD error: no Typo3 instance detected');

/**
 * Add Backend Layout with 4 columns.
 *
 * Must be activated in the Typo3 backend page>edit>Appearance>Backend Layout
 * The colPos variables must correspond to the values set in uzh_cd_2023/Configuration/TypoScript/lib/0030_content.typoscript [Line:189]
**/

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
        mod.web_layout.BackendLayouts {
          uzhCd4Column {
            title = UZH CD - 4 column
            icon = EXT:uzh_cd_2023/ext_icon.gif
            config {
              backend_layout {
                colCount = 4
                rowCount = 1
                rows {
                  1 {
                    columns {
                      1 {
                        name = left
                        colPos = 1
                      }
                      2 {
                        name = Normal
                        colPos = 0
                      }
                      3 {
                        name = Right
                        colPos = 2
                      }
                      4 {
                        name = Border
                        colPos = 3
                      }
                    }
                  }
                }
              }
            }
          }
        }
');
