<?php
defined('TYPO3') or
defined('TYPO3_MODE') or
die('UZH CD error: no Typo3 instance detected');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'uzh_cd_2023',
    'Configuration/TypoScript',
    'UZH Corporate Design'
);
